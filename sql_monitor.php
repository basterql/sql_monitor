<?php

if ($_REQUEST["action"] == "add"){
	$mysqli = new mysqli("127.0.0.1", "log", 'pass');
	if ($mysqli->connect_errno) {
		echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}


	$rows = [];
	$res = $mysqli->query("SHOW FULL PROCESSLIST");
	while($row = $res->fetch_assoc())
	{
		$rows[] = $row;
	}
	$html = [];
	$i = 1;
	foreach ($rows as $query) {
		if (!empty($query["Info"]) == true && $query["Info"] !='SHOW FULL PROCESSLIST') {
			$html[$i]['Info'] = $query['Info'];
			$html[$i]['Id'] = $query['Id'];
			$html[$i]['User'] = $query['User'];
			$html[$i]['Time'] = $query['Time'];
			$i++;
		}
	}
	header( 'Content-type: application/x-json', true );
	echo json_encode( $html );

	exit;
}


?><html>
<head>
	<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<style>
		.refresh_button {
			margin-left: 50px;
		}
	</style>
</head>
<body style="background: #eee;">
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<select>
					<option data-timeout="1000"> 1</option>
					<option data-timeout="5000" selected> 5</option>
					<option data-timeout="10000"> 10</option>
					<option data-timeout="15000"> 15</option>
					<option data-timeout="30000"> 30</option>
					<option data-timeout="60000"> 60</option>
				</select>
			</div>
			<div class="col-md-4">
				<input id="reload" type="checkbox">
				<label for="reload"> Не обновлять</label>
				<a href="#" class="refresh_button">Обновить</a>
			</div>
		</div>
		<div class="col-md-12 sql">

		</div>
	</div>
</section>
</body>
<script>

	function watcher_helper(){

		setTimeout(function () {
			$.ajax({
				method: 'post',
				dataType: 'json',
				url: "/sql_monitor.php?action=add",
				success: function (result) {
					var i = 1;

					$('.sql').text('');
					$.each(result, function() {
						$('.sql').append('<div class = "col-md-12" style="background-color: #e5dede; margin: 20px; padding: 10px"><pre style="background-color: #fff; padding:20px">' + result[i].Info + "</pre>" +"ID=>" + result[i].Id + ';  USER=>' + result[i].User+ ';  TIME=>' + result[i].Time  + '</div>');
						i++;
					});

					if ($("#reload").prop("checked") == false){

						watcher();

					}

				}
			});
		}, $("option:selected").data("timeout"));

	}

	function watcher() {

		if ($("#reload").prop("checked") == true){
			return;
		}


		watcher_helper();


	}
	$(function () {

		watcher();

		$(document).on("change", "#reload", function () {
			watcher();
		});

		$('.refresh_button').click(function(event){

			event.preventDefault();

			watcher_helper();

		});

	});


</script>
</html>